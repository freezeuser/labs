import unittest
from main_freeze.freeze import Freeze

date_format = '%Y-%m-%d %H:%M:%S'


class CommandLineTestCase(unittest.TestCase):

    freeze = None

    @classmethod
    def setUpClass(cls):
        freeze = Freeze()
        cls.freeze = freeze
        cls.freeze.restart_daemon = cls.mock_daemon

    @classmethod
    def mock_daemon(cls):
        print('restart daemon')

    def setUp(self):
        self.freeze._config_parser()

    @classmethod
    def tearDownClass(cls):
        cls.freeze._config_parser()


class DefaultTestCase(CommandLineTestCase):

    def test_no_args(self):
        """
        User passes zero args, must raise SystemExit.
        """

        with self.assertRaises(SystemExit):
            self.freeze.process_parser([])

    def test_wrong_main_command(self):
        """
        User passes wrong main command, must raise SystemExit.
        """
        with self.assertRaises(SystemExit):
            self.freeze.process_parser(['freeze', 'wrong_command'])

    def test_wrong_user_command(self):
        """
        User passes wrong user command, must raise SystemExit.
        """
        with self.assertRaises(SystemExit):
            self.freeze.process_parser(['freeze', 'user', 'wrong_command'])

    def test_wrong_task_command(self):
        """
        User passes wrong task command, must raise SystemExit.
        """
        with self.assertRaises(SystemExit):
            self.freeze.process_parser(['freeze', 'task', 'wrong_command'])

    def test_wrong_task_schedule_command(self):
        """
        User passes wrong task schedule command, must raise SystemExit.
        """
        with self.assertRaises(SystemExit):
            self.freeze.process_parser(['freeze', 'task', 'schedule', 'wrong_command'])

    def test_wrong_task_history_command(self):
        """
        User passes wrong task history command, must raise SystemExit.
        """
        with self.assertRaises(SystemExit):
            self.freeze.process_parser(['freeze', 'task', 'history', 'wrong_command'])

    def test_wrong_copy_user_tasks_command(self):
        """
        User passes wrong user copy command, must raise SystemExit.
        """
        with self.assertRaises(SystemExit):
            self.freeze.process_parser(['freeze', 'user', 'copy', '--full'])


class AddTaskTestCase(CommandLineTestCase):

    freeze = None

    @classmethod
    def setUpClass(cls):
        super(AddTaskTestCase, cls).setUpClass()
        cls.freeze.create_user('test_user', 'test_profile')
        cls.freeze.change_current_user(user_index=1)

    @classmethod
    def tearDownClass(cls):
        cls.freeze.clear_user_list()

    def setUp(self):
        super().setUp()
        self.freeze.current_user.clear_task_list()
        self.freeze.save_user_list()

    def test_add_task_without_deadline(self):
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_task'))

        self.assertEqual(len(self.freeze.current_user.task_dict), 1)
        self.assertEqual(self.freeze.current_user.task_dict[1].name, 'test_task')

    def test_cli_add_task_without_deadline(self):
        self.freeze.process_parser(['freeze', 'task', 'create', '-n', 'test_task'])

        self.assertEqual(len(self.freeze.current_user.task_dict), 1)
        self.assertEqual(self.freeze.current_user.task_dict[1].name, 'test_task')

    def test_add_sub_task_without_deadline(self):
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_task'))
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_sub_task'), task_id=1)

        self.assertEqual(len(self.freeze.current_user.task_dict[1].task_dict), 1)
        self.assertEqual(self.freeze.current_user.task_dict[1].task_dict[2].name, 'test_sub_task')

    def test_cli_add_sub_task_without_deadline(self):
        self.freeze.process_parser(['freeze', 'task', 'create', '-n', 'test_task'])
        self.freeze._config_parser()
        self.freeze.process_parser(['freeze', 'task', 'create', '-id', '1', '-n', 'test_sub_task'])

        self.assertEqual(len(self.freeze.current_user.task_dict[1].task_dict), 1)
        self.assertEqual(self.freeze.current_user.task_dict[1].task_dict[2].name, 'test_sub_task')

    def test_add_sub_task_without_with_wrong_id(self):
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_task'))
        with self.assertRaises(SystemExit):
            self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_sub_task'), task_id=5)

    def test_cli_add_sub_task_with_wrong_id(self):
        self.freeze.process_parser(['freeze', 'task', 'create', '-n', 'test_task'])
        self.freeze._config_parser()

        with self.assertRaises(SystemExit):
            self.freeze.process_parser(['freeze', 'task', 'create', '-id', '5', '-n', 'test_sub_task'])

    def test_add_task_with_deadline(self):
        today = '1880-12-12 12:12:12'
        tomorrow = '1990-12-12 12:12:12'
        task_obj = self.freeze.current_user.create_task('test_task', date_start=today, date_end=tomorrow)
        self.freeze.current_user.add_task(task_obj)

        self.assertEqual(len(self.freeze.current_user.task_dict), 1)

    def test_add_sub_task_with_deadline(self):
        today = '1880-12-12 12:12:12'
        tomorrow = '1990-12-12 12:12:12'
        task_obj = self.freeze.current_user.create_task('test_task', date_start=today, date_end=tomorrow)
        self.freeze.current_user.add_task(task_obj)
        task_obj2 = self.freeze.current_user.create_task('test_sub_task', date_start=today, date_end=tomorrow)
        self.freeze.current_user.add_task(task_obj2, task_id=1)

        self.assertEqual(len(self.freeze.current_user.task_dict[1].task_dict), 1)
        self.assertEqual(self.freeze.current_user.task_dict[1].task_dict[2].name, 'test_sub_task')

    def test_cli_add_task_with_deadline(self):
        self.freeze.process_parser(['freeze', 'task', 'create', '-n', 'test_task', '-ds',
                                 '1880-12-12', '12:12:12', '-de', '1990-12-12', '12:12:12'])

        self.assertEqual(len(self.freeze.current_user.task_dict), 1)

    def test_cli_add_sub_task_with_deadline(self):
        self.freeze.process_parser(['freeze', 'task', 'create', '-n', 'test_task', '-ds',
                                  '1880-12-12', '12:12:12', '-de', '1990-12-12', '12:12:12'])
        self.freeze._config_parser()
        self.freeze.process_parser(['freeze', 'task', 'create', '-id', '1', '-n', 'test_sub_task', '-ds',
                                  '1880-12-12', '12:12:12', '-de', '1990-12-12', '12:12:12'])

        self.assertEqual(len(self.freeze.current_user.task_dict), 1)
        self.assertEqual(self.freeze.current_user.task_dict[1].task_dict[2].name, 'test_sub_task')

    def test_add_task_with_deadline_with_wrong_date(self):
        pass

    def test_cli_add_task_with_deadline_with_wrong_date(self):
        with self.assertRaises(SystemExit):
            self.freeze.process_parser(['freeze', 'task', 'create', '-n', 'test_task', '-ds',
                                      '1880-12-12', '12:12:12', '-de', '1990-12-12', 'wrong'])

    def test_add_task_with_priority_level(self):
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_task', priority_level=1))

        self.assertEqual(len(self.freeze.current_user.task_dict), 1)
        self.assertEqual(self.freeze.current_user.task_dict[1].priority_level, 1)

    def test_cli_add_task_with_priority_level(self):
        self.freeze.process_parser(['freeze', 'task', 'create', '-n', 'test_task', '-pl', '1'])

        self.assertEqual(len(self.freeze.current_user.task_dict), 1)
        self.assertEqual(self.freeze.current_user.task_dict[1].priority_level, 1)

    def test_add_task_with_wrong_priority_level(self):
        with self.assertRaises(SystemExit):
            self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_task', priority_level=10))

    def test_cli_add_task_with_wrong_priority_level(self):

        with self.assertRaises(SystemExit):
            self.freeze.process_parser(['freeze', 'task', 'create', '-n', 'test_task', '-pl', '10'])


class AddUserTestCase(CommandLineTestCase):

    def setUp(self):
        self.freeze.clear_user_list()
        self.freeze._config_parser()

    @classmethod
    def tearDownClass(cls):
        cls.freeze.clear_user_list()

    def test_add_user(self):
        new_user = self.freeze.create_user('test_user')
        self.freeze.add_user(new_user)

        self.assertEqual(len(self.freeze._user_list), 2)
        self.assertEqual(self.freeze._user_list[1].name, 'test_user')

    def test_cli_add_user(self):
        self.freeze.process_parser(['freeze', 'user', 'create', '-n', 'test_user'])

        self.assertEqual(len(self.freeze._user_list), 2)
        self.assertEqual(self.freeze._user_list[1].name, 'test_user')


class RemoveTaskTestCase(CommandLineTestCase):

    freeze = None

    @classmethod
    def setUpClass(cls):
        super(RemoveTaskTestCase, cls).setUpClass()
        cls.freeze.create_user('test_user', 'test_profile')
        cls.freeze.change_current_user(user_index=1)

    @classmethod
    def tearDownClass(cls):
        cls.freeze.clear_user_list()

    def setUp(self):
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_task'))
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_sub_task'), task_id=1)
        self.freeze._config_parser()

    def tearDown(self):
        self.freeze.current_user.clear_task_list()
        self.freeze.current_user.task_count = 0

    def test_remove_task(self):
        self.freeze.current_user.remove_task_by_id(task_id=1)

        self.assertEqual(len(self.freeze.current_user.task_dict), 0)

    def test_cli_remove_task(self):
        self.freeze.process_parser(['freeze', 'task', 'remove', '-id', '1'])

        self.assertEqual(len(self.freeze.current_user.task_dict), 0)

    def test_remove_sub_task(self):
        self.freeze.current_user.remove_task_by_id(task_id=2)

        self.assertEqual(len(self.freeze.current_user.task_dict[1].task_dict), 0)

    def test_cli_remove_sub_task(self):
        self.freeze.process_parser(['freeze', 'task', 'remove', '-id', '2'])

        self.assertEqual(len(self.freeze.current_user.task_dict[1].task_dict), 0)

    def test_remove_with_wrong_id(self):

        with self.assertRaises(SystemExit):
            self.freeze.current_user.remove_task_by_id(task_id=5)

    def test_cli_remove_with_wrong_id(self):

        with self.assertRaises(SystemExit):
            self.freeze.process_parser(['freeze', 'task', 'remove', '-id', '5'])

    def test_clear_task_list(self):
        self.freeze.current_user.clear_task_list()

        self.assertEqual(len(self.freeze.current_user.task_dict), 0)

    def test_cli_clear_task_list(self):
        self.freeze.process_parser(['freeze', 'task', 'remove', '--all'])

        self.assertEqual(len(self.freeze.current_user.task_dict), 0)


class RemoveUserTestCase(CommandLineTestCase):

    def setUp(self):
        self.freeze.clear_user_list()
        self.freeze.create_user(name='test_user')
        self.freeze.change_current_user(user_index=0)
        self.freeze._config_parser()

    @classmethod
    def tearDownClass(cls):
        cls.freeze.clear_user_list()

    def test_clear_user_list(self):
        self.freeze.clear_user_list()

        self.assertEqual(len(self.freeze._user_list), 1)

    def test_remove_user(self):
        self.freeze.remove_current_user()

        self.assertEqual(len(self.freeze._user_list), 1)
        self.assertEqual(self.freeze.current_user.name, 'Guest')

    def test_cli_remove_user(self):
        self.freeze.process_parser(['freeze', 'user', 'remove'])

        self.assertEqual(len(self.freeze._user_list), 1)
        self.assertEqual(self.freeze.current_user.name, 'Guest')


class ChangeUserTestCase(CommandLineTestCase):

    @classmethod
    def setUpClass(cls):
        super(ChangeUserTestCase, cls).setUpClass()
        cls.freeze.clear_user_list()
        cls.freeze.add_user(cls.freeze.create_user(name='start_user'))
        cls.freeze.add_user(cls.freeze.create_user(name='test_user'))
        cls.freeze._config_parser()

    def setUp(self):
        self.freeze.change_current_user(user_index=0)
        self.freeze._config_parser()

    @classmethod
    def tearDownClass(cls):
        cls.freeze.clear_user_list()

    def test_change_user(self):
        self.freeze.change_current_user(user_index=3)

        self.assertEqual(self.freeze.current_user.name, 'test_user')

    def test_cli_change_user(self):
        self.freeze.process_parser(['freeze', 'user', 'change', '-id', '3'])

        self.assertEqual(self.freeze.current_user.name, 'test_user')


class ConfigTestCase(CommandLineTestCase):

    def setUp(self):
        self.freeze.config.set_default_config()

    def test_notify_on_startup_config(self):
        self.assertFalse(self.freeze.config.config_args.get('notify_on_startup'))

        self.freeze.notify_on_startup(notify=True)

        self.assertTrue(self.freeze.config.config_args.get('notify_on_startup'))

    def test_cli_notify_on_startup_config(self):
        self.assertFalse(self.freeze.config.config_args.get('notify_on_startup'))

        self.freeze.process_parser(['freeze', 'config', '--notify-on-startup'])

        self.assertTrue(self.freeze.config.config_args.get('notify_on_startup'))


class EditTaskTestCase(CommandLineTestCase):

    @classmethod
    def setUpClass(cls):
        super(EditTaskTestCase, cls).setUpClass()
        cls.freeze.create_user('test_user', 'test_profile')
        cls.freeze.change_current_user(user_index=1)

    @classmethod
    def tearDownClass(cls):
        cls.freeze.clear_user_list()

    def setUp(self):
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_task'))
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_sub_task'), task_id=1)
        self.freeze._config_parser()

    def tearDown(self):
        self.freeze.current_user.clear_task_list()
        self.freeze.current_user.task_count = 0

    def test_cli_edit_task(self):
        self.freeze.process_parser(['freeze', 'task', 'edit', '-id', '1', '-n', 'test_edit', '-ds',
                                  '2000-12-12', '12:12:12', '-de', '2001-12-12', '12:12:12'])

        self.assertEqual(self.freeze.current_user.task_dict[1].name, 'test_edit')

    def test_edit_task(self):
        task_obj = self.freeze.current_user.create_task(name='test_edit')
        self.freeze.current_user.edit_task_by_id(task_obj, 1)

        self.assertEqual(self.freeze.current_user.task_dict[1].name, 'test_edit')

    def test_cli_edit_task_with_wrong_id(self):
        with self.assertRaises(SystemExit):
            self.freeze.process_parser(['freeze', 'task', 'edit', '-id', '5', '-n', 'test_edit', '-ds',
                                      '2000-12-12', '12:12:12', '-de', '2001-12-12', '12:12:12'])

    def test_edit_task_with_wrong_id(self):
        with self.assertRaises(SystemExit):
            task_obj = self.freeze.current_user.create_task(name='test_edit')
            self.freeze.current_user.edit_task_by_id(task_obj, 5)

    def test_cli_full_edit_task(self):
        self.freeze.process_parser(['freeze', 'task', 'edit', '-id', '1', '-n', 'test_edit', '-ds',
                                  '2000-12-12', '12:12:12', '-de', '2001-12-12', '12:12:12', '--full'])

        self.assertEqual(self.freeze.current_user.task_dict[1].name, 'test_edit')
        self.assertEqual(len(self.freeze.current_user.task_dict[1].task_dict), 0)

    def test_full_edit_task(self):
        task_obj = self.freeze.current_user.create_task(name='test_edit')
        self.freeze.current_user.edit_task_by_id(task_obj, 1, full=True)

        self.assertEqual(self.freeze.current_user.task_dict[1].name, 'test_edit')
        self.assertEqual(len(self.freeze.current_user.task_dict[1].task_dict), 0)


class EditUserTestCase(CommandLineTestCase):

    def setUp(self):
        self.freeze.clear_user_list()
        self.freeze.create_user(name='test_user')
        self.freeze.change_current_user(user_index=0)
        self.freeze._config_parser()

    @classmethod
    def tearDownClass(cls):
        cls.freeze.clear_user_list()

    def test_edit_user(self):
        self.freeze.current_user.edit_name('edit_name')

        self.assertEqual(self.freeze.current_user.name, 'edit_name')

    def test_cli_edit_user(self):
        self.freeze.process_parser(['freeze', 'user', 'edit', '-n', 'edit_name'])

        self.assertEqual(self.freeze.current_user.name, 'edit_name')


class LinkTasksTestCase(CommandLineTestCase):

    freeze = None

    @classmethod
    def setUpClass(cls):
        super(LinkTasksTestCase, cls).setUpClass()
        cls.freeze.create_user('test_user', 'test_profile')
        cls.freeze.change_current_user(user_index=1)

    @classmethod
    def tearDownClass(cls):
        cls.freeze.clear_user_list()

    def setUp(self):
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_task'))
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_sub_task'), task_id=1)
        self.freeze._config_parser()

    def tearDown(self):
        self.freeze.current_user.clear_task_list()
        self.freeze.current_user.task_count = 0

    def test_delete_linked_tasks(self):
        self.freeze.current_user.link_tasks(1, 2)

        self.freeze._config_parser()
        with self.assertRaises(SystemExit):
            self.freeze.process_parser(['freeze', 'task', 'remove', '-id', '1'])

    def test_cli_delete_linked_tasks(self):
        self.freeze.process_parser(['freeze', 'task', 'link', '1', '2'])

        self.freeze._config_parser()
        with self.assertRaises(SystemExit):
            self.freeze.process_parser(['freeze', 'task', 'remove', '-id', '1'])


class CommentTasksTestCase(CommandLineTestCase):
    freeze = None

    @classmethod
    def setUpClass(cls):
        super(CommentTasksTestCase, cls).setUpClass()
        cls.freeze.create_user('test_user', 'test_profile')
        cls.freeze.change_current_user(user_index=1)

    @classmethod
    def tearDownClass(cls):
        cls.freeze.clear_user_list()

    def setUp(self):
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_task'))
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_sub_task'), task_id=1)
        self.freeze._config_parser()

    def tearDown(self):
        self.freeze.current_user.clear_task_list()
        self.freeze.current_user.task_count = 0

    def test_add_comment(self):
        self.freeze.current_user.add_comment(1, 'test_comment')

        self.assertEqual(self.freeze.current_user.task_dict[1].comments_list[0], ('Guest', 'test_comment'))
        self.assertEqual(len(self.freeze.current_user.task_dict[1].comments_list), 1)

    def test_cli_add_comment(self):
        self.freeze.process_parser(['freeze', 'task', 'comment', '-id', '1', '--comment', 'test_comment'])

        self.assertEqual(self.freeze.current_user.task_dict[1].comments_list[0], ('Guest', 'test_comment'))
        self.assertEqual(len(self.freeze.current_user.task_dict[1].comments_list), 1)


class HistoryTestCase(CommandLineTestCase):

    freeze = None

    @classmethod
    def setUpClass(cls):
        super(HistoryTestCase, cls).setUpClass()
        cls.freeze.create_user('test_user', 'test_profile')
        cls.freeze.change_current_user(user_index=1)
        cls.freeze.current_user.history.clear_history()

    @classmethod
    def tearDownClass(cls):
        cls.freeze.clear_user_list()
        cls.freeze._config_parser()

    def setUp(self):
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_task'))
        self.freeze.current_user.add_task(self.freeze.current_user.create_task(name='test_sub_task'), task_id=1)
        self.freeze._config_parser()

    def tearDown(self):
        self.freeze.current_user.clear_task_list()
        self.freeze.current_user.task_count = 0
        self.freeze.current_user.history.clear_history()

    def test_task_history(self):
        self.freeze.current_user.remove_task_by_id(1)

        self.assertEqual(len(self.freeze.current_user.history.task_history), 1)
        self.assertEqual(self.freeze.current_user.history.task_history[0][2].name, 'test_task')

    def test_restore_task_by_id_history(self):
        self.freeze.current_user.remove_task_by_id(1)

        self.freeze.current_user.restore_task_history(task_id=1)

        self.assertEqual(self.freeze.current_user.task_dict[1].name, 'test_task')

