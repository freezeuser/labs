import main_freeze.core.system_tools as system_tools
import os
import json
import logging

logger = logging.getLogger(__name__)


class Config(object):
    """
    Class for system folders setup and parse/create default config files.s
    """

    def __init__(self, system_path=None, users_path=None, config_path=None):
        logger.info('Config class start')

        self.config_folder = system_path
        self.users_path = users_path
        self.config_path = config_path

        self.config_args = dict()

        self._create_system_folders()
        self._process_config()

    def _create_system_folders(self):
        """
        Creates main_freeze/system and main_freeze/config folders.
        """
        logger.info('Creating system folders')
        if self.config_folder is None:
            self.config_folder = os.path.join('main_freeze', 'system', 'config')
        if not os.path.exists(self.config_folder):
            os.makedirs(self.config_folder)
        self._create_system_files()

    def _create_system_files(self):
        """
        Creates config.json to store config_args and users.json to store user's info.
        """
        logger.info('Creating system files')

        if self.users_path is None:
            self.users_path = os.path.join(self.config_folder, 'users.json')

        if self.config_path is None:
            self.config_path = os.path.join(self.config_folder, 'config.json')

        system_tools.create_file(self.config_path)
        system_tools.create_file(self.users_path)

    def _process_config(self):
        """
        Parse config file and store info in config_args.
        """
        logger.info('Parse config file')

        config_str = system_tools.get_file_as_string(self.config_path)
        try:
            self.config_args = json.loads(config_str)
        except Exception:
            self.set_default_config()

    def set_default_config(self):
        """
        Setup default config file.
        """
        logger.info('Setting default config')

        data = dict()
        data.update({'current_user': 0})
        data.update({'notify_on_startup': False})
        data.update({'is_running': False})

        with open(self.config_path, 'w') as config_file:
            json.dump(data, config_file)
        self._process_config()

    def save_config(self):
        """
        Save config into file.
        """
        logger.info('Save config file')

        with open(self.config_path, 'w') as config_file:
            json.dump(self.config_args, config_file)

    def print_config(self):
        """
        Printing config to the terminal.
        """
        logger.info('Printing config file')
        print('Config file:')
        for key, value in self.config_args.items():
            print(f'{key} - {value}')
