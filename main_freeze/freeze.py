import argparse
import datetime
import sys
import json
import jsonpickle
import logging
import platform
import main_freeze.config as config
import main_freeze.core.user as user
import main_freeze.core.system_tools as system_tools
import main_freeze.daemon as daemon
import main_freeze.cli_tools as cli

logging.basicConfig (level=logging.INFO,
                     format='%(asctime)s %(levelname)-8s %(message)s',
                     datefmt='%Y-%m-%d %H:%M:%S',
                     filename='log1.log', filemode='w')
# logging.disable(level=logging.INFO)

logger = logging.getLogger (__name__)


class Freeze (object):
    """
    Main class of tracker
    """

    def __init__(self, system_path=None, users_path=None, config_path=None, pidfile='/tmp/freeze.pid'):

        logger.info ('Program start')
        logging.warning ('Program start')

        self.config = config.Config (system_path, users_path, config_path)

        self._user_list = list ()
        self.current_user = None

        self.platform = platform.system ()
        self.pidfile = pidfile

        logger.info ('Start loading user list')
        self.load_user_list ()

        if not self._user_list:
            logger.info ('Empty user list')
            self.create_default_user ()

        if self.config.config_args['notify_on_startup']:
            cli.print_task_dict (self.current_user.task_dict, is_notify=True)

        self.parser = None
        self.args = None
        self._sys_argv = None
        self._config_parser ()

    def create_default_user(self):
        logger.info ('Creating default user')

        self._user_list.append (user.User (name='Guest'))
        self.current_user = self._user_list[0]

    def load_user_list(self):
        """
        Decodes users.json file in system folder into _user_list.
        """
        logger.info ('Loading user list')

        users_json = system_tools.get_file_as_string (self.config.users_path)
        try:
            self._user_list = jsonpickle.decode (users_json, keys=True)
            current_user_index = self.config.config_args['current_user']
            self.current_user = self._user_list[current_user_index]
        except Exception:
            logger.info ('Failed to load user list')
            self.create_default_user ()

        logging.info ('Finish loading user list')

    def save_user_list(self, users_path=None):
        """
        Saves _user_list to the users.json and beautify it.
        """
        logging.info ('Start saving user list')

        parsed = jsonpickle.encode (self._user_list, keys=True)
        load = json.loads (parsed)

        if users_path is None:
            with open (self.config.users_path, 'w') as users_file:
                users_file.write (json.dumps (load, indent=4, sort_keys=True))
        else:
            with open (users_path, 'w') as users_file:
                users_file.write (json.dumps (load, indent=4, sort_keys=True))

        logging.info ('Finish saving user list')

    def add_user(self, user_obj):
        """
        Appends user to _user_list.

        :param user_obj:
        :type user_obj: User
        """

        logging.info ('Adding user to the user_list')

        type_validation = isinstance (user_obj, user.User)

        if type_validation:
            for user_item in self._user_list:
                if user_item.name == user_obj.name:
                    raise SystemExit ('User with this name already exist')
            self._user_list.append (user_obj)

    def remove_user(self, user_obj):
        """
        Removes user_obj from _user_list.

        :param user_obj:
        :type user_obj: User
        """
        logging.info ('Removing user from the user_list')

        type_validation = isinstance (user_obj, user.User)

        if type_validation:
            self._user_list.remove (user_obj)

    def remove_current_user(self):
        """
        Removes current user from the _user_list and switch current user to the first in the _user_list
        """
        logging.info ('Removing current user from the user_list')

        self._user_list.remove (self.current_user)
        if self._user_list:
            self.current_user = self._user_list[0]
        else:
            self.create_default_user ()

    @staticmethod
    def create_user(name=None, profile=None):
        """
        Creates user with specified name and profile and save it.

        :param name:
        :type name: str
        :param profile:
        :type profile: str
        """
        logging.info ('Creating user')

        new_user = user.User ()
        new_user.name = name
        new_user.profile = profile

        return new_user

    def show_all_users(self):
        """
        Prints all users in a readable form.
        """
        logging.info ('Showing all users')

        for num in range (len (self._user_list)):
            print ("{0}. {1}.".format (num + 1, self._user_list[num].name))

    def change_current_user(self, user_index=None):
        """
        Change user to user on user_index in _user_list.

        :param user_index:
        :type user_index: int
        :raises SystemExit: If wrong index
        """
        logging.info ('Changing current user')
        if user_index < 0:
            raise SystemExit ('Index can not be less than zero')
        try:
            self.current_user = self._user_list[int (user_index) - 1]
            self.config.config_args['current_user'] = int (user_index) - 1
        except IndexError:
            self.current_user = self._user_list[0]
            self.config.config_args['current_user'] = 0
            self.config.save_config ()
            raise SystemExit ("Wrong input")

        self.config.save_config ()

    def _config_parser(self):
        """
        Configuring parser for freeze command
        """
        logging.info ('Configure main parser')

        self.parser = argparse.ArgumentParser (description='Pretends to be good task tracker',
                                               usage='''freeze <command> [<args>]

   Commands:
   user     Record changes to the repository
   task      Download objects and refs from another repository
   dashboard Print information about current user
   config Configure config file
   start Start daemon
   stop Stop daemon
''')
        self.parser.add_argument ('command', type=str, help='Sub command to run')

    def process_parser(self, args=None):
        """
        Method for processing args from outer call or sys.argv.

        :param args: Args from outer method call
        :type args: list
        :raises SystemExit: if command does not exist.
        """
        logging.info ('Processing main parser')

        if not (args is None):
            self._sys_argv = args
        else:
            self._sys_argv = sys.argv
        self.args = self.parser.parse_args (self._sys_argv[1:2])
        try:
            if not len (sys.argv) > 1 and len (args) == 0:
                raise SystemExit ('Zero args')
        except TypeError:
            raise SystemExit ('Zero args')

        logging.warning ('Processing command in main parser: {0}'.format (self.args.command))

        daemon_flag = False

        if self.args.command == 'user':
            self._user_command ()
        elif self.args.command == 'task':
            self._task_command ()
        elif self.args.command == 'dashboard':
            self._dashboard_command ()
        elif self.args.command == 'config':
            self._config_command ()
        elif self.args.command == 'start':
            self.start_daemon ()
            daemon_flag = True
        elif self.args.command == 'stop':
            self.stop_daemon ()
            daemon_flag = True
        else:
            raise SystemExit ('Wrong command')

        self.save_user_list ()
        if not daemon_flag and self.platform == 'Linux':
            self.restart_daemon ()

    def _config_command(self):
        """
        Configure parser to process config arguments.
        """
        self.parser = argparse.ArgumentParser (description='Config command', usage='''freeze config [<args>]

   Args:
   --show-config: Print whole config to the terminal
   --reset Reset config to default
   --notify-on-startup If set -> output of deadline tasks on each command
''')

        self.parser.add_argument ('--show-config', action='store_true')
        self.parser.add_argument ('--reset', action='store_true')
        self.parser.add_argument ('--notify-on-startup', action='store_true')

        self._process_config_arguments ()

    def _process_config_arguments(self):
        """
        Processing of config arguments in self.args.
        """
        self.args = self.parser.parse_args (self._sys_argv[2:])
        if self.args.reset:
            self.current_user.task_dict.clear ()
            self.current_user.task_count = 0
        self.notify_on_startup (self.args.notify_on_startup)
        self.config.save_config ()

    def notify_on_startup(self, notify=False):
        """
        Setting notify on startup config var to notify value.
        :param notify:
        :type notify: bool
        :return:
        """
        if notify:
            if self.config.config_args.get ('notify_on_startup'):
                self.config.config_args.update ({'notify_on_startup': False})
            else:
                self.config.config_args.update ({'notify_on_startup': True})

    def _user_command(self):
        """
        Configure parser to process user arguments.
        """
        self.parser = argparse.ArgumentParser (description='User command', usage='''freeze user <command> [<args>]

   Commands:
   show Showing current user
   remove Removing current user
   create Creating user
   list Output all users
   copy Copy tasks from one user to another
   link todo
   comment Comment any other user task
   edit Edit name of the current user
''')

        self.parser.add_argument ('command', type=str, help='Sub command to run')

        self._process_user_parser ()

    def _process_user_parser(self):
        """
        Processing user command.

        :raises SystemExit: if command does not exist
        """
        self.args = self.parser.parse_args (self._sys_argv[2:3])

        logging.warning ('Processing command in user parser: {0}'.format (self.args.command))

        if self.args.command == 'show':
            self._user_show_command ()
        elif self.args.command == 'remove':
            self._user_remove_command ()
        elif self.args.command == 'create':
            self._user_create_command ()
        elif self.args.command == 'list':
            self._user_list_command ()
        elif self.args.command == 'change':
            self._user_change_command ()
        elif self.args.command == 'copy':
            self._user_copy_command ()
        elif self.args.command == 'link':
            self._user_link_command ()
        elif self.args.command == 'comment':
            self._user_comment_command ()
        elif self.args.command == 'edit':
            self._user_edit_command ()
        else:
            raise SystemExit ('Wrong command')

    def _user_edit_command(self):
        """
        Configuring user edit command parser.
        """
        self.parser = argparse.ArgumentParser (description='User edit')

        self.parser.add_argument ('-n', '--name', required=True, help='New user name')

        self._process_user_edit_command ()

    def _process_user_edit_command(self):
        """
        Processing user edit command arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])

        self.current_user.edit_name (self.args.name)

    def _user_comment_command(self):
        """
        Configuring user comment command parser.
        """
        self.parser = argparse.ArgumentParser (description='User comment')

        self.parser.add_argument ('-user_id', type=int, required=True, help='User id')
        self.parser.add_argument ('-task_id', type=int, required=True, help='Task id')
        self.parser.add_argument ('--comment', nargs='*', type=str, required=True, help='Comment')

        self._process_user_comment_arguments ()

    def _process_user_comment_arguments(self):
        """
        Processing user comment arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])

        if self.args.user_id <= 0:
            raise SystemExit ('User id can not be less or equal to zero')

        try:
            target_user = self._user_list[self.args.user_id - 1]
        except ValueError:
            raise SystemExit ('There is no such a user id')

        search_task = target_user.find_task_by_id (self.args.task_id)

        if search_task is None:
            raise SystemExit ('There is no task with this id')

        comment = " ".join (self.args.comment)

        search_task.comments_list.append ((self.current_user.name, comment))

    def _user_copy_command(self):
        """
        Configuring user copy command parser.
        """
        self.parser = argparse.ArgumentParser ()

        self.parser.add_argument ('-user_id', type=int, required=True, help='User id')
        self.parser.add_argument ('-task_id', type=int, default=None, help='Task id')
        self.parser.add_argument ('--full', action='store_true', help='If we need full copy')

        self._process_user_copy_arguments ()

    def _process_user_copy_arguments(self):
        """
        Processing user copy arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])

        self.copy_user_tasks (user_id=self.args.user_id, task_id=self.args.task_id, full=self.args.full)

    def copy_user_tasks(self, user_id, task_id, full=False):
        """
        Copying tasks from user specified on user_id.
        :param user_id:
        :type user_id: int
        :param task_id:
        :type task_id: int
        :param full:
        :type full: bool
        """
        try:
            target_user = self._user_list[user_id - 1]
        except ValueError:
            raise SystemExit ('No user with this id.')

        if task_id is None and not full:
            raise SystemExit ('Wrong command')

        if full:
            target_tasks = target_user.task_dict
            for key, value in target_tasks.items ():
                self.current_user.add_task (value)
        else:
            target_task = target_user.find_task_by_id (task_id)
            if target_task is None:
                raise SystemExit ('Can not find task with this id')
            self.current_user.add_task (target_task)

    def _user_link_command(self):
        """
        Configuring user link command parser.
        """
        self.parser = argparse.ArgumentParser ()

        self.parser.add_argument ('-user_id', type=int, required=True, help='User id')
        self.parser.add_argument ('-task_id', type=int, required=True, help='Task id')
        self.parser.add_argument ('--allow', action='store_true', help='If we need to allow to remove task')

        self._process_user_link_arguments ()

    def _process_user_link_arguments(self):
        """
        Processing user link arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])

        target_user = self._user_list[self.args.user_id - 1]
        target_task = target_user.find_task_by_id (self.args.task_id)

        if self.args.allow:
            # TODO: Implement allow
            pass
        else:
            target_task.permissions.append ((self.current_user.name, False))

    def _user_show_command(self):
        """
        Configure parser for user show command.
        """
        self.parser = argparse.ArgumentParser (description='User show')

        self.parser.add_argument ('-id', type=int, default=None, help='User id')

        self._process_user_show_arguments ()

    def _process_user_show_arguments(self):
        """
        Process user show arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])
        if self.args.id is not None:
            try:
                target_user = self._user_list[self.args.id - 1]
            except IndexError:
                raise SystemExit ('Wrong user id')
            print ('User: {0}'.format (target_user.name))
            cli.print_task_dict (target_user.task_dict)
        else:
            self.print_current_user ()

    def _user_remove_command(self):
        """
        Configure parser for user remove command.
        """
        self.parser = argparse.ArgumentParser (description='User remove')

        self._process_user_remove_arguments ()

    def _process_user_remove_arguments(self):
        """
        Process user remove arguments.
        """
        logging.warning ('Removing current user')

        self.remove_current_user ()

    def _user_create_command(self):
        """
        Configure parser for user create command.
        """
        self.parser = argparse.ArgumentParser (description='User create')

        self.parser.add_argument ('-n', '--name', nargs='?', type=str, default='Default', help='New user name')
        self.parser.add_argument ('-p', '--profile', nargs='?', type=str,
                                  default='Default profile', help='New user profile')

        self._process_user_create_arguments ()

    def _process_user_create_arguments(self):
        """
        Process user create arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])

        new_user = self.create_user (self.args.name, self.args.profile)

        logging.warning ('Add user with: {0} name'.format (self.args.name))

        self.add_user (new_user)

    def _user_list_command(self):
        """
        Configure parser for user list command.
        """
        self.parser = argparse.ArgumentParser (description='User list')

        self._process_user_list_arguments ()

    def _process_user_list_arguments(self):
        """
        Process arguments for user list command.
        """
        self.show_all_users ()

    def _user_change_command(self):
        """
        Configure parser for user change command.
        """
        self.parser = argparse.ArgumentParser (description='User change')

        self.parser.add_argument ('-id', default=1, type=int, help='User id')
        self._process_user_change_arguments ()

    def _process_user_change_arguments(self):
        """
        Process user change arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])

        logging.warning ('Changing user to the {0} id'.format (self.args.id))

        self.change_current_user (user_index=self.args.id)

    def _task_command(self):
        """
        Configure parser for task command.
        """
        self.parser = argparse.ArgumentParser (description='Task parser', usage='''freeze task <command> [<args>]

   Commands:
   remove  Removing task
   create  Creating task
   edit Editing task
   complete Completing task
   tag Tag any task
   search Search any task by tags and name
   notify Creating notify on task
   link Linking 2 tasks
   schedule Schedule command
   comment Comment any task
''')

        self.parser.add_argument ('command', type=str, help='Sub command to run')

        self._process_task_parser ()

    def _process_task_parser(self):
        """
        Process for task command.
        """

        self.args = self.parser.parse_args (self._sys_argv[2:3])

        logging.warning ('Processing command in task parser: {0}'.format (self.args.command))

        if self.args.command == 'remove':
            self._task_remove_command ()
        elif self.args.command == 'create':
            self._task_create_command ()
        elif self.args.command == 'edit':
            self._task_edit_command ()
        elif self.args.command == 'complete':
            self._task_complete_command ()
        elif self.args.command == 'tag':
            self._task_tag_command ()
        elif self.args.command == 'search':
            self._task_search_command ()
        elif self.args.command == 'history':
            self._task_history_command ()
        elif self.args.command == 'notify':
            self._task_notify_command ()
        elif self.args.command == 'link':
            self._task_link_command ()
        elif self.args.command == 'schedule':
            self._task_schedule_command ()
        elif self.args.command == 'comment':
            self._task_comment_command ()
        elif self.args.command == 'share':
            self._task_share_command ()
        else:
            raise SystemExit ('Command does not exist')

    def _task_share_command(self):
        """
        Configure task share command parser.
        """

        self.parser = argparse.ArgumentParser ()

        self.parser.add_argument ('-user_id', type=int, required=True, help='User id')
        self.parser.add_argument ('-task_id', type=int, required=True, help='Task id')

        self._process_task_share_arguments ()

    def _process_task_share_arguments(self):
        """
        Process task share arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])

        try:
            target_user = self._user_list[self.args.user_id - 1]
        except IndexError:
            raise SystemExit ('This user does not exist !')

        target_task = self.current_user.find_task_by_id (self.args.task_id)

        if target_task is None:
            raise SystemExit ('This task does not exist')
        if (target_user.name, False) not in target_task.permissions:
            target_task.permissions.append ({'name': target_user.name, 'permission': False})
        else:
            raise SystemExit ('Task already shared')

        target_task.is_shared = True
        target_user.add_task (target_task)

    def _task_comment_command(self):
        """
        Configure task comment command parser.
        """
        self.parser = argparse.ArgumentParser (description='Task comment')

        self.parser.add_argument ('-id', type=int, required=True, help='Task id')
        self.parser.add_argument ('--comment', type=str, required=True, nargs='*', help='Task comment')

        self._process_task_comment_arguments ()

    def _process_task_comment_arguments(self):
        """
        Process task comment arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])

        comment = " ".join (self.args.comment)

        self.current_user.add_comment (self.args.id, comment)

    def _task_schedule_command(self):
        """
        Configure task schedule command parser.
        """
        self.parser = argparse.ArgumentParser (description='Task schedule', usage='''freeze <command> [<args>]

   Commands:
   list     Output schedule list
   init     Init any schedule task
   create   Creating any schedule data on any task
   reset    Reset schedule data
''')

        self.parser.add_argument ('command', type=str, help='Sub command to run')

        self._process_task_schedule_arguments ()

    def _process_task_schedule_arguments(self):
        """
        Process task schedule arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:4])

        logging.warning ('Processing command in task schedule parser: {0}'.format (self.args.command))

        if self.args.command == 'list':
            self._task_schedule_list_command ()
        elif self.args.command == 'init':
            self._task_schedule_init_command ()
        elif self.args.command == 'create':
            self._task_schedule_create_command ()
        elif self.args.command == 'reset':
            self._task_schedule_reset_command ()
        else:
            raise SystemExit ('Wrong command')

    def _task_schedule_reset_command(self):
        self.current_user.scheduler.clear_scheduler_data ()

    def _task_schedule_list_command(self):
        """
        Configure task schedule list command parser.
        """
        self.parser = argparse.ArgumentParser (description='Schedule list')

        self.parser.add_argument ('--full', action='store_true', help='If we need full output')

        self._process_task_schedule_list_arguments ()

    def _process_task_schedule_list_arguments(self):
        """
        Process task schedule list command arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[4:])
        cli.print_schedule_tasks (self.current_user.scheduler.schedule_data, self.args.full)

    def _task_schedule_init_command(self):
        """
        Configure task schedule init command parser.
        """
        self.parser = argparse.ArgumentParser (description='Schedule init')

        self.parser.add_argument ('-id', type=int, required=True, help='Schedule id')

        self._process_task_schedule_init_arguments ()

    def _process_task_schedule_init_arguments(self):
        """
        Process task schedule init arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[4:])

        self.current_user.init_schedule_task (self.args.id)

    def _task_schedule_create_command(self):
        """
        Configure task schedule create command parser.
        """
        self.parser = argparse.ArgumentParser (description='Schedule create')

        self.parser.add_argument ('id', type=int, help='Task id')
        self.parser.add_argument ('-y', '--year', type=int, default=None, help='Years')
        self.parser.add_argument ('-mn', '--month', type=int, default=None, help='Months')
        self.parser.add_argument ('-d', '--day', type=int, default=0, help='Days')
        self.parser.add_argument ('-hr', '--hour', type=int, default=0, help='Hours')
        self.parser.add_argument ('-mt', '--minute', type=int, default=0, help='Minutes')
        self.parser.add_argument ('-ds', '--date_start', nargs=2, type=str, default=None, help='Date start')
        self.parser.add_argument ('--repeat_count', type=int, default=None, help='Repeat count')

        self._process_task_schedule_create_arguments ()

    def _process_task_schedule_create_arguments(self):
        """
        Process task schedule create arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[4:])

        repeat_time = 0

        if not (self.args.year is None):
            repeat_time += self.args.year * 365

        if not (self.args.month is None):
            repeat_time += self.args.month * 30

        repeat_time += self.args.day + (self.args.hour / 24) + (self.args.minute / 1440)

        if repeat_time <= 0:
            raise SystemExit ('Repeat time can not be less than zero')
        if not (self.args.repeat_count is None):
            if self.args.repeat_count <= 0:
                raise SystemExit ('Repeat count can not be less than zero')

        if self.args.date_start is not None:
            date_start = " ".join (self.args.date_start)
        else:
            date_start = None

        if self.current_user.task_dict.get (self.args.id) is None:
            raise SystemExit ('Can not create scheduled sub task')

        self.current_user.schedule_task (self.args.id, repeat_time, self.args.repeat_count, date_start)

    def _task_link_command(self):
        """
        Configure task link command parser.
        """
        self.parser = argparse.ArgumentParser (description='Task link')

        self.parser.add_argument ('first_task_id', type=int, help='First task id')
        self.parser.add_argument ('second_task_id', type=int, help='Second task id')

        self._process_task_link_arguments ()

    def _process_task_link_arguments(self):
        """
        Process task link arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])

        logging.warning ('Linking current user tasks with: {0} and {1} id'
                         .format (self.args.first_task_id, self.args.second_task_id))

        self.current_user.link_tasks (self.args.first_task_id, self.args.second_task_id)

    def _task_notify_command(self):
        """
        Configure task notify command parser.
        """
        self.parser = argparse.ArgumentParser (description='Task link')

        self.parser.add_argument ('id', type=int, help='Task id')
        self.parser.add_argument ('-y', '--year', type=int, default=None, help='Years')
        self.parser.add_argument ('-mn', '--month', type=int, default=None, help='Months')
        self.parser.add_argument ('-d', '--day', type=int, default=0, help='Days')
        self.parser.add_argument ('-hr', '--hour', type=int, default=0, help='Hours')
        self.parser.add_argument ('-mt', '--minute', type=int, default=0, help='Minutes')

        self._process_task_notify_arguments ()

    def _process_task_notify_arguments(self):
        """
        Process task notify arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])
        notify_data = (self.args.day, self.args.hour, self.args.minute)
        if not (self.args.year is None) and not (self.args.month is None):
            try:
                notify_data = datetime.datetime (year=self.args.year, month=self.args.month, day=self.args.day,
                                                 hour=self.args.hour, minute=self.args.minute)
            except Exception:
                raise SystemExit ('Wrong data')
            if datetime.datetime.now () > notify_data:
                raise SystemExit ('Wrong data')

        logging.warning ('Processing task notify with notify_data: {0}'.format (notify_data))

        self.current_user.add_notify_to_task (self.args.id, notify_data)

    def _task_history_command(self):
        """
        Configure parser for task history command.
        """
        self.parser = argparse.ArgumentParser (description='Task history', usage='''freeze task history <command> [<args>]

   Commands:
   list     Print whole history
   restore  Restore task from history
   remove Remove task from history
''')

        self.parser.add_argument ('command')

        self._process_task_history_arguments ()

    def _process_task_history_arguments(self):
        """
        Process task history arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:4])

        logging.warning ('Processing command in task history parser: {0}'.format (self.args.command))

        if self.args.command == 'list':
            self._task_history_list_command ()
        elif self.args.command == 'restore':
            self._task_history_restore_command ()
        elif self.args.command == 'remove':
            self._task_history_reset_command ()
        else:
            raise SystemExit ('Wrong command')

    def _task_history_reset_command(self):
        """
        Configure task history reset command parser.
        """
        self.parser = argparse.ArgumentParser (description='Task history reset')

        self.parser.add_argument ('-id', type=int, required=True, help='History task id')
        self.parser.add_argument ('--all', action='store_true', help='If we need to restore whole history')

        self._process_task_history_reset_command ()

    def _process_task_history_reset_command(self):
        """
        Process task history reset command.
        """
        self.args = self.parser.parse_args (self._sys_argv[4:])

        if self.args.id is None and not self.args.all:
            raise SystemExit ('Wrong task history reset arguments')
        else:
            self.current_user.remove_task_history (self.args.id, self.args.all)

    def _task_history_list_command(self):
        """
        Configure task history list command.
        """
        self.parser = argparse.ArgumentParser (description='Task history list')

        self.parser.add_argument ('--full', action='store_true', help='If we need to full output')

        self._process_task_history_list_arguments ()

    def _process_task_history_list_arguments(self):
        """
        Process task history list arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[4:])
        cli.print_task_history (self.current_user, self.args.full)

    def _task_history_restore_command(self):
        """
        Configure task history restore command parser.
        """

        self.parser = argparse.ArgumentParser (description='Task history restore')

        self.parser.add_argument ('-id', type=int, required=True, help='Task history id')
        self.parser.add_argument ('--all', action='store_true', help='If we need to full restore')

        self._process_task_history_restore_arguments ()

    def _process_task_history_restore_arguments(self):
        """
        Process task history restore arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[4:])

        logging.warning ('Processing task history command with: {0} id'.format (self.args.id))

        self.current_user.restore_task_history (self.args.id, self.args.all)

    def _task_search_command(self):
        """
        Configure parser for task search command.
        """
        self.parser = argparse.ArgumentParser (description='Task search')

        self.parser.add_argument ('-n', '--name', type=str, required=True, help='Task name')
        self.parser.add_argument ('-tr', '--tree', action='store_true', help='If we need full output')
        self.parser.add_argument ('-t', '--tag', type=str, nargs='+', default=None, help='Task tags')

        self._process_task_search_arguments ()

    def _process_task_search_arguments(self):
        """
        Process task search arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])

        logging.warning ('Processing task search command with: {0} name, {1} tags arguments'
                         .format (self.args.name, self.args.tag))
        cli.print_task_list_by_request (self.current_user.task_dict, self.args.name, self.args.tag, self.args.tree)

    def _task_tag_command(self):
        """
        Configure parser for task tag command.
        """
        self.parser = argparse.ArgumentParser (description='Task tag')

        self.parser.add_argument ('id', type=int, help='Task id')
        self.parser.add_argument ('-r', '--remove', action='store_true', help='If we need to remove tags')
        self.parser.add_argument ('tag', type=str, nargs='+', default=None, help='Tags')

        self._process_task_tag_arguments ()

    def _process_task_tag_arguments(self):
        """
        Configure task tag parser.
        """
        self.args = self.parser.parse_args (self._sys_argv[2:])

        logging.warning ('Processing task tag with: {0} tags and {1} id'.format (self.args.tag, self.args.id))

        self.current_user.append_tags_by_id (self.args.id, self.args.tag, self.args.remove)

    def _task_complete_command(self):
        """
        Configure parser for task complete command.
        """
        self.parser = argparse.ArgumentParser (description='Task complete')
        self.parser.add_argument ('-id', type=int, help='Task id')
        self.parser.add_argument ('-f', '--force', action='store_true', help='If we need force complete')

        self._process_task_complete_arguments ()

    def _process_task_complete_arguments(self):
        """
        Process task complete arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])

        self.current_user.remove_task_by_id (self.args.id, self.args.force)
        self.current_user.loyalty_points += 5

    def _task_remove_command(self):
        """
        Configure parser for task remove command.
        """
        self.parser = argparse.ArgumentParser (description='Task remove')

        self.parser.add_argument ('-id', default=0, type=int, help='Task id')
        self.parser.add_argument ('-l', '--list', action='store_true', help='Task list')
        self.parser.add_argument ('--all', action='store_true', help='If we need to remove all tasks')

        self._process_task_remove_arguments ()

    def _process_task_remove_arguments(self):
        """
        Process task remove arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])

        logging.warning ('Processing task remove: {0} id'.format (self.args.id))

        if self.args.list:
            cli.print_task_dict (self.current_user.task_dict)

        if self.args.all:
            self.current_user.clear_task_list ()
        else:
            self.current_user.remove_task_by_id (self.args.id, user_name=self.current_user.name)

    def _task_edit_command(self):
        """
        Configure parser for task edit command.
        """
        self.parser = argparse.ArgumentParser (description='Task edit')

        self.parser.add_argument ('-id', default=None, type=int, help='Task id')
        self.parser.add_argument ('--full', action='store_true', help='If we need to full replace')
        self.parser.add_argument ('-n', '--name', type=str, help='New task name')
        self.parser.add_argument ('-ds', '--date_start', nargs=2, type=str, default=None, help='New date start')
        self.parser.add_argument ('-de', '--date_end', nargs=2, type=str, default=None, help='New date end')

        self._process_task_edit_arguments ()

    def _process_task_edit_arguments(self):
        """
        Process task edit arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])

        if not (self.args.date_start is None):
            date_start = " ".join (self.args.date_start)
        else:
            date_start = None
        if not (self.args.date_end is None):
            date_end = " ".join (self.args.date_end)
        else:
            date_end = None

        logging.warning (
            'Editing task with: {0} name, date_start {1}, date_end {2}'.format (self.args.name, date_start, date_end))

        task_obj = self.current_user.create_task (self.args.name, date_start, date_end)
        self.current_user.edit_task_by_id (task_obj, self.args.id, self.args)

    def _task_create_command(self):
        """
        Configure parser for task create command.
        """
        self.parser = argparse.ArgumentParser (description='Task create')

        self.parser.add_argument ('-id', default=None, type=int, help='Id if we need to add sub task')
        self.parser.add_argument ('-n', '--name', type=str, required=True, help='Task name')
        self.parser.add_argument ('-ds', '--date_start', nargs=2, type=str, default=None, help='Task date start')
        self.parser.add_argument ('-de', '--date_end', nargs=2, type=str, default=None, help='Task date end')
        self.parser.add_argument ('-pl', '--priority-level', type=int, default=1, help='Task priority level')

        self._process_task_create_arguments ()

    def _process_task_create_arguments(self):
        """
        Process task create arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[3:])
        if not (self.args.date_start is None):
            date_start = " ".join (self.args.date_start)
        else:
            date_start = None
        if not (self.args.date_end is None):
            date_end = " ".join (self.args.date_end)
        else:
            date_end = None

        logging.warning ('Creating task with: {0} name, date_start {1}, date_end {2}'
                         .format (self.args.name, date_start, date_end))

        task_obj = self.current_user.create_task (self.args.name, date_start, date_end, self.args.priority_level)

        self.current_user.add_task (task_obj, self.args.id, self.args)

    def _dashboard_command(self):
        """
        Configure parser for dashboard command.
        """
        self.parser = argparse.ArgumentParser (description='Dashboard')

        self.parser.add_argument ('--full', action='store_true', help='If we need full output')
        self.parser.add_argument ('--calendar', action='store_true', help='If we need calendar')

        self._process_dashboard_arguments ()

    def _process_dashboard_arguments(self):
        """
        Process dashboard arguments.
        """
        self.args = self.parser.parse_args (self._sys_argv[2:])

        self.print_current_user_dashboard (full=self.args.full)
        if self.args.calendar:
            print ('\\\\\\\\\\\\\\\\\\\\\\\\')
            self.print_current_user_calendar ()

    def create_task(self, name='default', date_start=None, date_end=None):
        """
        Shortcut for create_task method.
        :param name:
        :type name: str
        :param date_start:
        :type date_start: datetime
        :param date_end:
        :type date_end: datetime
        :return: Task
        """
        return self.current_user.create_task (name, date_start, date_end)

    def add_task(self, task_obj, task_id=None, args=None):
        """
        Shortcut for add_task method.
        :param task_obj:
        :type task_obj: Task
        :param task_id:
        :type task_id: int
        :param args: Optional arguments
        :type args: Namespace
        :return:
        """
        self.current_user.add_task (task_obj, task_id, args)

    def link_tasks(self, first_id, second_id):
        """
        Shortcut for link tasks method.
        :param first_id:
        :type first_id: int
        :param second_id:
        :type second_id: int
        :return:
        """
        self.current_user.link_tasks (first_id, second_id)

    def print_current_user(self):
        print ("Current user: {0}".format (self.current_user.name))

    def print_current_user_calendar(self):
        """
        Shortcut for printing current user calendar.
        """
        cli.print_calendar (self.current_user)

    def print_current_user_dashboard(self, full=False):
        """
        Printing current user dashboard.
        :param full: Printing full info about tasks.
        :type full: bool
        :return:
        """
        print ('User: {0}'.format (self.current_user.name))
        print ('Task count: {0}'.format (self.current_user.task_count))
        print ('Loyalty points: {0}'.format (self.current_user.loyalty_points))
        cli.print_task_dict (self.current_user.task_dict, full)

    def clear_user_list(self):
        """
        Clearing user list
        """
        self._user_list.clear ()
        self.create_default_user ()

    def restart_daemon(self):
        if self.platform == 'Linux':
            dm = daemon.NotificationDaemon (self, self.pidfile)
            dm.restart ()
        else:
            print ('notifications are not available on this system')

    def start_daemon(self):
        if self.platform == 'Linux':
            dm = daemon.NotificationDaemon (self, self.pidfile)
            dm.restart ()
        else:
            print ('notifications are not available on this system')

    def stop_daemon(self):
        if self.platform == 'Linux':
            dm = daemon.NotificationDaemon (self, self.pidfile)
            dm.stop ()
        else:
            print ('notifications are not available on this system')


def main():
    freeze = Freeze ()
    freeze.process_parser ()


if __name__ == "__main__":
    main ()
