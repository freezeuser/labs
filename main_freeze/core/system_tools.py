import os
import time
import datetime
import argparse


def create_file(path):
    """
    Creates file on specified path.

    :param path: Path to the file
    :type path: str
    """
    if not os.path.exists(path):
        file = open(path, 'a+')
        file.close()


def get_file_as_string(path):
    """
    :param path: Path to the file
    :type path: str
    :return: file as str
    :rtype: str
    """
    if os.path.exists(path):
        with open(path, 'r') as file:
            return file.read()


def is_file_empty(path):
    """
    :param path: Path to the file
    :type path: str
    :return: True if empty or False
    :rtype: bool
    """
    return os.stat(path).st_size == 0


def cli_query_yes_no(question):
    """
    Ask user in terminal to perform any action.

    :param question: Action description
    :type question: str
    :return: True if yes, False if no
    :rtype: bool
    """
    yes = {'yes', 'ye', 'y'}

    choice = input(question + ' [Yes/No]: ').lower()
    if choice in yes:
        return True
    else:
        return False


def valid_time_string(s):
    try:
        return time.strptime(s, "%H:%M:%S")
    except ValueError:
        msg = "Not a valid time: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def valid_datetime_string(s):
    try:
        return datetime.datetime.strptime(s, '%Y-%m-%d %H:%M:%S')
    except ValueError:
        msg = "Not a valid datetime: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def valid_count_string(s):
    try:
        int(s)
        if s <= 0:
            msg = "Count needs to be >0"
            raise argparse.ArgumentTypeError(msg)
        else:
            return s
    except ValueError:
        msg = "Not a valid count: '{0}'.".format(s)
        raise argparse.ArgumentParser(msg)
