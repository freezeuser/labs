class Scheduler(object):

    def __init__(self):
        self.task_count = 1
        self.schedule_data = dict()

    def add_schedule_task(self, task_obj, repeat_time, repeat_count, date_start, task_id):
        """
        Add schedule data tuple to the class dict.

        :param task_obj: Task obj that we need to schedule.
        :type task_obj: Task
        :param repeat_time:
        :type repeat_time: int
        :param repeat_count:
        :type repeat_count: int
        :param date_start:
        :type date_start: datetime
        :param task_id:
        :type task_id: int
        """

        self.schedule_data[self.task_count] = {'task': task_obj, 'repeat_time': repeat_time,
                                               'repeat_count': repeat_count,
                                               'date_start': date_start, 'task_id': task_id}

        self.task_count += 1

    def get_schedule_task(self, task_id):
        schedule_task = self.schedule_data.get(task_id)

        if schedule_task is None:
            raise SystemExit('No such a schedule task')
        else:
            return schedule_task

    def clear_scheduler_data(self):
        self.schedule_data.clear()


class ScheduledData(object):

    def __init__(self, repeat_time, repeat_count, date_now, task, task_id):
        self.repeat_time = repeat_time
        self.repeat_count = repeat_count
        self.date_now = date_now
        self.task = task
        self.task_id = task_id