import main_freeze.core.task as task
import main_freeze.core.history as history
import main_freeze.core.scheduler as scheduler
import datetime
import re
import copy
import logging

logger = logging.getLogger(__name__)


class User(object):
    """ Class for representing user.
    """

    def __init__(self, name="placeholder"):
        logger.info('Creating user')

        self.name = name
        self.profile = 'simple profile'
        self.loyalty_points = 0
        self.task_count = 0

        self.task_dict = dict()

        self.history = history.History()
        self.scheduler = scheduler.Scheduler()

    @staticmethod
    def create_task(name=None, date_start=None, date_end=None, priority_level=None):
        """
         Method for creating task from the data in arguments.

        :param name: task.name
        :type name: str
        :param date_start:
        :type date_start: datetime
        :param date_end:
        :type date_end: datetime
        :param priority_level:
        :type priority_level: int
        :return: Task object
        :rtype: Task
        """
        logger.info('Task create')

        date_pattern = '^\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01]) ' \
                       '(00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9]):([0-9]|[0-5][0-9])$'
        dt = re.compile(date_pattern)

        if not (date_start is None or date_end is None):
            date_start = dt.match(date_start)
            date_end = dt.match(date_end)
            if date_start is None or date_end is None:
                raise SystemExit('Wrong date input')
            else:
                date_start = date_start.group(0)
                date_end = date_end.group(0)

        task_obj = task.Task(name, date_start, date_end)
        if not (priority_level is None):
            if priority_level > task.PriorityLevel.HIGH or priority_level < task.PriorityLevel.LOW:
                logger.info('Wrong priority level exception')
                raise SystemExit('Wrong priority level (only 1-3 is allowed)')
            else:
                task_obj.priority_level = priority_level

        return task_obj

    def add_task(self, task_obj, task_id=None, args=None):
        """
        :param task_obj:
        :type task_obj: Task
        :param task_id:
        :type task_id: int
        :param args: Optional arguments
        """
        logger.info('Appending task')

        self.task_count = self.task_count + 1

        if not (task_id is None):
            self.add_task_by_id(task_obj, task_id, args)
        else:
            self.task_dict[self.task_count] = task_obj

    def add_task_by_id(self, task_obj, task_id, args):
        """
        Add sub task on specified task id.

        :param task_obj:
        :type task_obj: Task
        :param task_id:
        :type task_id: int
        :param args: Optional arguments
        :raises SystemExit: When there is no task with task_id
        """
        logger.info('Appending task by id')
        search_task = self.find_task_by_id(task_id)

        if not (search_task is None):
            search_task.task_dict[self.task_count] = task_obj
        else:
            raise SystemExit('There is no such a task')

    def edit_task_by_id(self, task_obj, task_id, full=False):
        """
        Replace fields or whole task object on task_obj

        :param task_obj:
        :type task_obj: Task
        :param task_id: task id that method need to edit
        :type task_id: int
        :param full:
        :type full: bool
        :raises SystemExit: When there is no task with specified task_id
        """
        logger.info('Editing task by id')

        search_task = self.find_task_by_id(task_id)
        if not (search_task is None):
            search_task.name = task_obj.name
            search_task.date_start = task_obj.date_start
            search_task.date_end = task_obj.date_end
            search_task.date_edit = datetime.datetime.now()
            if full:
                search_task.task_dict.clear()
        else:
            raise SystemExit('There is no')

    def remove_task_by_id(self, task_id, user_name=None):
        """
        :param task_id:
        :type task_id: int
        :param user_name:
        :type user_name: str
        """
        logger.info('Removing task by id')

        if not (self.task_dict.get(task_id) is None):
            for link in self.task_dict[task_id].linked_tasks:
                if not (self.find_task_by_id(task_id=link) is None):
                    raise SystemExit('Cannot delete this task, linked tasks: {0}'
                                     .format(self.task_dict[task_id].linked_tasks))
            deleted_obj = copy.deepcopy(self.task_dict[task_id])
            if self.task_dict[task_id].is_shared:
                shared_flag = False
                for item in self.task_dict[task_id].permissions:
                    if item['name'] == user_name:
                        shared_flag = True
                        item['permission'] = True
                        break
                if shared_flag:
                    if self.task_dict[task_id].check_permission():
                        del self.task_dict[task_id]
                    else:
                        raise SystemExit('Sub tasks contain unresolved permissions')
                if not shared_flag:
                    for item in self.task_dict[task_id].permissions:
                        if not item['permission']:
                            raise SystemExit('Can not delete this task cause of other users.')
                    if self.task_dict[task_id].check_permission():
                        del self.task_dict[task_id]
                    else:
                        raise SystemExit('Sub tasks contain unresolved permissions')
            else:
                if self.task_dict[task_id].check_permission():
                    del self.task_dict[task_id]
                else:
                    raise SystemExit('Sub tasks contain unresolved permissions')
                self.history.add_task((None, task_id, deleted_obj))
        else:
            for key, task_item in self.task_dict.items():
                deleted_obj = task_item.remove_task_by_id(task_id, key)
                if not(deleted_obj[2] is None):
                    self.history.add_task(deleted_obj)
                    break
                else:
                    raise SystemExit('Wrong id')

    def edit_name(self, name):
        if isinstance(name, str):
            self.name = name
        else:
            raise SystemExit('Wrong name type.')

    def append_tags_by_id(self, task_id, tags, remove=False):
        """
        Append or removing (depends on remove flag) tags from task's tag list.

        :param task_id:
        :type task_id: int
        :param tags:
        :type tags: list
        :param remove:
        :type remove: bool
        :raises SystemExit: When trying to remove tags that not exist in tag list
        """
        logger.info('Appending tags by id')

        if not (self.task_dict.get(task_id) is None):
            if remove:
                try:
                    for tag in tags:
                        self.task_dict[task_id].tag_list.remove(tag)
                except ValueError:
                    raise SystemExit('There is no such a tag {0}'.format(tag))
            else:
                self.task_dict[task_id].tag_list.extend(tags)
        else:
            for key, task_item in self.task_dict.items():
                if task_item.append_tags_by_id(task_id, tags):
                    break

    def find_task_by_id(self, task_id):
        """
        :param task_id:
        :return: task or None
        :rtype: Task
        """
        logger.info('Finding task by id')

        if not (self.task_dict.get(task_id) is None):
            return self.task_dict[task_id]
        else:
            for key, task_item in self.task_dict.items():
                search_task = task_item.find_task_by_id(task_id)
                if not (search_task is None):
                    return search_task
        return None

    def restore_task_history(self, task_id=None, full=False):
        """
        Restoring task from the history object.

        :param task_id: id of task to restore.
        :type task_id: int
        :param full: Flag to restore full history
        :type full: bool
        """
        if full:
            history_tasks = self.history.get_all_tasks()
            for history_task in history_tasks:
                self.restore_task(history_task[2], history_task[1], history_task[0])
        else:
            history_task = self.history.get_task(task_id)
            self.restore_task(history_task[2], history_task[1], history_task[0])

    def restore_task(self, task_obj, task_id, parent_id):
        """
        Restoring task_obj on task_id and parent_id.

        :param task_obj: Task to restore
        :type task_obj: Task
        :param task_id: Id which task_obj will have.
        :type task_id: int
        :param parent_id:
        :type parent_id: int
        :return:
        """
        if parent_id is None:
            if not (task_id in self.task_dict):
                self.task_dict[task_id] = copy.deepcopy(task_obj)
            #    self.history.task_history.remove(task_obj)
        else:
            parent_task = self.find_task_by_id(parent_id)
            if parent_task is None:
                raise SystemExit('There is no parent task. Can not restore this task')
            else:
                if parent_task.task_dict[task_id] is None:
                    parent_task.task_dict[task_id] = copy.deepcopy(task_obj)
                    self.history.task_history.remove(task_obj)
                else:
                    raise SystemExit('Task on this id already exist. Can not restore this task')

    def remove_task_history(self, task_id, full=False):
        """
        Removing task history.

        :param task_id: Id of task to remove from history
        :type task_id: int
        :param full: Flag to clear history
        :type full: bool
        :return:
        """
        if full:
            self.history.task_history.clear()
        else:
            self.history.remove_task(task_id)

    def get_temporary_task_list(self):
        """
        :return: list with tasks that has is_temporary flag
        """
        task_list = list()
        for key, value in self.task_dict.items():
            if value.is_temporary or len(value.notify_list) > 0:
                task_list.append(value)
            child_list = value.get_temporary_task_list()
            task_list.extend(child_list)
        return task_list

    def get_scheduled_task_list(self):
        """
        :return: list with tasks that has any scheduled_data
        """
        task_list = list()
        for key, value in self.task_dict.items():
            if not (value.scheduled_data is None):
                task_list.append(value)
            child_list = value.get_scheduled_task_list()
            task_list.extend(child_list)
        return task_list

    def check_permission(self):

        for key, item in self.task_dict.items():
            for permission in item.permissions:
                if not permission['permission']:
                    return False
            if not item.check_permission():
                return False
        return True

    def add_comment(self, task_id, comment, user_name=None):
        """
        Append comment to comments_list of the task on task_id

        :param task_id:
        :type task_id: int
        :param comment:
        :type comment: str
        :param user_name:
        :type user_name: str
        """
        search_task = self.find_task_by_id(task_id)
        if search_task is None:
            raise SystemExit('Wrong task id')
        else:
            if user_name is None:
                user_name = self.name

            search_task.comments_list.append((user_name, comment))

    def link_tasks(self, first_id, second_id):
        """
        Linking tasks on second_id with task on first_id

        :param first_id:
        :type first_id: int
        :param second_id:
        :type second_id: int
        """
        first_task = self.find_task_by_id(first_id)
        second_task = self.find_task_by_id(second_id)

        if first_task is None or second_task is None:
            raise SystemExit('Wrong link request')
        else:
            if first_task.find_task_by_id(second_id) is not None or self.task_dict.get(second_id) is not None:
                if first_id in second_task.linked_tasks or second_id in first_task.linked_tasks:
                    raise SystemExit('Can not create link cycle')
                first_task.add_link(second_id)
            else:
                raise SystemExit('Can not link sub task to task')

    def add_notify_to_task(self, task_id, notify_data):
        """
        Adding notify to the task on specified task_id.

        :param task_id:
        :type task_id: int
        :param notify_data:
        :return:
        """
        search_task = self.find_task_by_id(task_id)

        if not (search_task is None):
            search_task.add_notify(notify_data)
        else:
            raise SystemExit('Wrong id')

    def schedule_task(self, task_id, repeat_time, repeat_count, date_start=None):
        """
        Schedule task on specified id in scheduler object

        :param task_id:
        :type task_id: int
        :param repeat_time:
        :type repeat_time: int
        :param repeat_count:
        :type repeat_count: int
        :param date_start:
        :type date_start: datetime
        :return:
        """
        search_task = self.find_task_by_id(task_id)

        if date_start is not None:
            try:
                datetime.datetime.strptime(date_start, task.date_format)
            except Exception:
                raise SystemExit('Wrong date')

        if search_task is None:
            raise SystemExit('Can not find task with this id')

        self.scheduler.add_schedule_task(search_task, repeat_time, repeat_count, date_start, task_id)

    def init_schedule_task(self, task_id):
        """
        Initialize schedule task specified on task_id in scheduler object.

        :param task_id:
        :type task_id: int
        :return:
        """
        scheduler_task = self.scheduler.get_schedule_task(task_id)
        target_task = self.find_task_by_id(task_id)

        if scheduler_task['date_start'] is None:
            date_now = datetime.datetime.now()
        else:
            date_now = datetime.datetime.strptime(scheduler_task['date_start'], task.date_format)

        if scheduler_task['task'] is None:
            raise SystemExit('Task does not exist')

        target_task.scheduled_data = scheduler.ScheduledData(scheduler_task['repeat_time'],
                                                             scheduler_task['repeat_count'],
                                                             date_now, scheduler_task['task'],
                                                             scheduler_task['task_id'])
     #   target_task.scheduled_data = {'repeat_time': scheduler_task['repeat_time'],
                               #       'repeat_count': scheduler_task['repeat_count'],
                                #      'date_now': date_now,
                                #      'task': scheduler_task['task'],
                                #      'task_id': scheduler_task['task_id']}

    def clear_task_list(self):
        self.task_count = 0
        self.task_dict.clear()
