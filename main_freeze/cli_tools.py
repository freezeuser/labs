import colorama
import datetime
import calendar
from main_freeze.core.task import PriorityLevel


def print_task(task_id, task_obj, full=False, indent_level=1):
    """
    Printing information about specified task in task_obj

    :param task_id:
    :type task_id: int
    :param task_obj:
    :type task_obj: Task
    :param indent_level:
    :type indent_level: int
    :param full: Boolean flag, if True print full information about task
    """

    indent = ' ' * indent_level

    if full and not (full is None):
        if task_obj.priority_level == PriorityLevel.LOW:
            print(f'{indent} {colorama.Fore.RED} Priority level: LOW {colorama.Style.RESET_ALL}')
        elif task_obj.priority_level == PriorityLevel.MEDIUM:
            print(f'{indent} {colorama.Fore.RED} Priority level: MEDIUM {colorama.Style.RESET_ALL}')
        elif task_obj.priority_level == PriorityLevel.HIGH:
            print(f'{indent} {colorama.Fore.RED} Priority level: HIGH {colorama.Style.RESET_ALL}')
        if task_obj.is_shared:
            print(f'{indent} {colorama.Fore.BLUE} SHARED {colorama.Style.RESET_ALL}')
        if len(task_obj.tag_list) > 0:
            print(f'{indent} Tags: {task_obj.tag_list}')
        for notify in task_obj.notify_list:
            print(f'{indent} {notify}')
        if len(task_obj.linked_tasks) > 0:
            print(f'{indent} Linked tasks: {task_obj.linked_tasks}')
        if len(task_obj.comments_list) > 0:
            print(f'{indent} {task_obj.comments_list}')
        if len(task_obj.permissions) > 0:
            print(task_obj.permissions)

        if task_obj.is_temporary:
            if datetime.datetime.now() >= task_obj.date_end:
                print(f'{indent}{task_id}. {colorama.Back.RED}ST{colorama.Style.RESET_ALL} {task_obj.name}')
            else:
                print(f'{indent}{task_id}. {colorama.Back.GREEN}ST{colorama.Style.RESET_ALL} {task_obj.name}')
            print(f'{indent}{colorama.Fore.GREEN} {task_obj.date_start} {colorama.Style.RESET_ALL} ->'
                  f'{colorama.Fore.RED} {task_obj.date_end} {colorama.Style.RESET_ALL}')
        else:
            print(f'{indent} {task_id}. {colorama.Back.BLUE}ST{colorama.Style.RESET_ALL} {task_obj.name}')
        print(f'{indent} Created: {task_obj.date_create}')
        if not (task_obj.date_edit is None):
            print(f'{indent} Edited: {task_obj.date_edit}')
    else:
        if task_obj.is_temporary:
            if datetime.datetime.now() >= task_obj.date_end:
                print(f'{indent}{task_id}. {colorama.Back.RED}ST{colorama.Style.RESET_ALL} {task_obj.name}')
            else:
                print(f'{indent}{task_id}. {colorama.Back.GREEN}ST{colorama.Style.RESET_ALL} {task_obj.name}')
            print(f'{indent}{colorama.Fore.GREEN} {task_obj.date_start} {colorama.Style.RESET_ALL} ->'
                  f'{colorama.Fore.RED} {task_obj.date_end} {colorama.Style.RESET_ALL}')
        else:
            print(f'{indent}{task_id}. {colorama.Back.BLUE}ST{colorama.Style.RESET_ALL} {task_obj.name}')
    print('------------------------------------------------------------------')


def print_task_dict(task_dict, full=False, is_notify=False, indent_level=1):
    """
    Printing task list recursively

    :param full: if True printing full information about all tasks
    :type full: bool or None
    :param task_dict:
    :type task_dict: dict
    :param is_notify: if True printing notifying version
    :type is_notify: bool
    :param indent_level:
    :type indent_level: int
    """
    colorama.init()
    if full is True:
        for key, value in sorted(task_dict.items()):
            print_task(key, value, full, indent_level)
            print_task_dict(value.task_dict, full=True, indent_level=indent_level+1)
    elif is_notify:
        for key, value in sorted(task_dict.items()):
            if value.is_temporary:
                print_task(key, value, indent_level=indent_level)
            print_task_dict(value.task_dict, False, is_notify, indent_level=indent_level+1)
    else:
        for key, value in sorted(task_dict.items()):
            print_task(key, value, indent_level=indent_level)
            print_task_dict(value.task_dict, full=False, indent_level=indent_level+1)
    colorama.deinit()


def print_task_list_by_request(task_dict, name, tags, tree=False):
    """
    Printing all tasks according to the name/tags request.

    :param name: task.name
    :type name: str
    :param tags: task.tag_list
    :type tags: list
    :param tree: Boolean flag, if True print whole task tree
    :type tree: bool
    :param task_dict:
    :type task_dict: dict
    :raises SystemExit: if name and tags is None
    """

    if not (name is None) and not (tags is None):
        for key, task_item in task_dict.items():
            if task_item.name == name and set(tags) <= set(task_item.tag_list):
                print_task(key, task_item)
                if tree:
                    print_task_dict(task_item.task_dict)
            task_item.print_task_list_by_request(name, tags, tree)
    elif name is None and not (tags is None):
        for key, task_item in task_dict.items():
            if set(tags) <= set(task_item.tag_list):
                print_task(key, task_item)
                if tree:
                    print_task_dict(task_item.task_dict)
            task_item.print_task_list_by_request(name, tags, tree)
    elif not (name is None) and tags is None:
        for key, task_item in task_dict.items():
            if task_item.name == name:
                print_task(key, task_item)
                if tree:
                    print_task_dict(task_item.task_dict)
            print_task_list_by_request(task_item.task_dict, name, tags, tree)
    else:
        raise SystemExit('Wrong request')


def print_task_history(user, full=False):
    """
    :param user:
    :type user: User
    :param full: Boolean flag to print full version of task list
    :type full: bool
    """

    print('->History<-')
    if not full and not (full is None):
        for task_tuple in user.history.task_history:
            if task_tuple[0] is not None:
                print(f'Parent id: {task_tuple[0]}')
            print(f'--> id: {task_tuple[1]} --> {task_tuple[2].name}')
    else:
        for task_tuple in user.history.task_history:
            if task_tuple[0] is not None:
                print(f'Parent id: {task_tuple[0]}')
            print_task(task_tuple[1], task_tuple[2], full=True)


def print_calendar(user):
    """
    Generate time stamps of temporary tasks of the current user and output it in calendar view.
    """
    red = 'RED'
    green = 'GREEN'
    yellow = 'YELLOW'
    now = datetime.datetime.now()
    month_calendar = calendar.monthcalendar(now.year, now.month)
    time_stamps = list()
    temporary_list = user.get_temporary_task_list()

    for task_item in temporary_list:
        if task_item.is_temporary:
            if task_item.date_end.month == now.month:
                time_stamps.append((task_item.date_end, red))
            if task_item.date_start.month == now.month:
                time_stamps.append((task_item.date_start, green))
        if len(task_item.notify_list) > 0:
            for notify in task_item.notify_list:
                if notify.month == now.month:
                    time_stamps.append((notify, yellow))
    for week in month_calendar:
        day_row = ''
        for day in week:
            is_stamp = False
            for stamp in time_stamps:
                if stamp[0].day == day:
                    is_stamp = True
                    if stamp[1] == red:
                        day_row += f'{colorama.Back.RED} {day} {colorama.Style.RESET_ALL}'
                    elif stamp[1] == green:
                        day_row += f'{colorama.Back.GREEN} {day} {colorama.Style.RESET_ALL}'
                    elif stamp[1] == yellow:
                        day_row += f'{colorama.Back.YELLOW} {day} {colorama.Style.RESET_ALL}'
            if not is_stamp:
                day_row += f' {day} '
        print(day_row)


def print_schedule_tasks(schedule_data, full=False):
    """
    Printing all current schedule tasks.

    :param schedule_data:
    :type schedule_data: dict
    :param full:
    :return:
    """
    for key, data in schedule_data.items():
        print('{0}: Task name: {1} | Repeat time: {2} | Repeat count: {3}'
              .format(key, data['task'].name, data['repeat_time'], data['repeat_count']))
