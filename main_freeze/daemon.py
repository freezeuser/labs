from __future__ import print_function
from main_freeze.core.task import PriorityLevel
import main_freeze.core.scheduler as scheduler
import sys
import os
import time
import atexit
import signal
import subprocess
import errno
import datetime
import copy


class Daemon(object):
    """
    A generic daemon class.
    """
    def __init__(self, pidfile, stdin=os.devnull,
                 stdout=os.devnull, stderr=os.devnull,
                 home_dir='.', umask=0o22, verbose=1,
                 use_gevent=False, use_eventlet=False):
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.pidfile = pidfile
        self.home_dir = home_dir
        self.verbose = verbose
        self.umask = umask
        self.daemon_alive = True
        self.use_gevent = use_gevent
        self.use_eventlet = use_eventlet

    def daemonize(self):
        """
        Do the UNIX double-fork magic, see Stevens' "Advanced
        Programming in the UNIX Environment" for details (ISBN 0201563177)
        http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
        """
        try:
            pid = os.fork()
            if pid > 0:
                # Exit first parent
                sys.exit(0)
        except OSError as e:
            sys.stderr.write(
                "fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1)

        # Decouple from parent environment
        os.chdir(self.home_dir)
        os.setsid()
        os.umask(self.umask)

        # Do second fork
        try:
            pid = os.fork()
            if pid > 0:
                # Exit from second parent
                sys.exit(0)
        except OSError as e:
            sys.stderr.write(
                "fork #2 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1)

        # Write pidfile
        atexit.register(
            self.del_pid)  # Make sure pid file is removed if we quit
        pid = str(os.getpid())
        open(self.pidfile, 'w+').write("%s\n" % pid)

    def del_pid(self):
        try:
            # the process may fork itself again
            pid = int(open(self.pidfile, 'r').read().strip())
            if pid == os.getpid():
                os.remove(self.pidfile)
        except OSError as e:
            if e.errno == errno.ENOENT:
                pass
            else:
                raise

    def start(self, *args, **kwargs):
        """
        Start the daemon
        """

        # Check for a pidfile to see if the daemon already runs
        try:
            pf = open(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None
        except SystemExit:
            pid = None

        if pid:
            sys.exit(1)

        # Start the daemon
        self.daemonize()
        self.run(*args, **kwargs)

    def stop(self):
        """
        Stop the daemon
        """

        # Get the pid from the pidfile
        pid = self.get_pid()

        if not pid:

            # Just to be sure. A ValueError might occur if the PID file is
            # empty but does actually exist
            if os.path.exists(self.pidfile):
                os.remove(self.pidfile)

            return  # Not an error in a restart

        # Try killing the daemon process
        try:
            i = 0
            while 1:
                os.kill(pid, signal.SIGTERM)
                time.sleep(0.1)
                i = i + 1
                if i % 10 == 0:
                    os.kill(pid, signal.SIGHUP)
        except OSError as err:
            if err.errno == errno.ESRCH:
                if os.path.exists(self.pidfile):
                    os.remove(self.pidfile)
            else:
                print(str(err))
                sys.exit(1)

    def restart(self):
        """
        Restart the daemon
        """
        self.stop()
        self.start()

    def get_pid(self):
        try:
            pf = open(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None
        except SystemExit:
            pid = None
        return pid

    def is_running(self):
        pid = self.get_pid()

        if pid is None:
            return False
        elif os.path.exists('/proc/%d' % pid):
            return True
        else:
            return False

    def run(self):
        """
        You should override this method when you subclass Daemon.
        It will be called after the process has been
        daemonized by start() or restart().
        """
        raise NotImplementedError


class NotificationDaemon(Daemon):
    """
    Class for creating notification daemon.
    """

    def __init__(self, freeze, pidfile, file_name=None):
        super().__init__(pidfile)
        self.freeze = freeze
        self.task_list = self.freeze.current_user.get_temporary_task_list()
        self.scheduled_list = self.freeze.current_user.get_scheduled_task_list()

        if file_name is None:
            self.file_name = 'temp.json'
        else:
            self.file_name = file_name

        self.file_path = os.path.join(self.freeze.config.config_folder, self.file_name)

    def run(self):
        while True:

            try:
                self.do_notification()
            except SystemExit:
                raise SystemExit()

            time.sleep(30)

    def do_notification(self):

        # Update scheduled_data and creating new task or not, according to this data.
        for task in self.scheduled_list:
            scheduled_data = task.scheduled_data

            if isinstance(scheduled_data.repeat_count, int):
                scheduled_data.repeat_count -= 1

            delta_time = scheduled_data.date_now + datetime.timedelta(days=scheduled_data.repeat_time)

            if scheduled_data.repeat_count is None:
                if datetime.datetime.now() > delta_time:
                    scheduled_data.date_now = delta_time
                    copy_task = copy.deepcopy(scheduled_data.task)
                    copy_task.scheduled_data = scheduler.ScheduledData(scheduled_data.repeat_time,
                                                                       scheduled_data.repeat_count,
                                                                       delta_time, scheduled_data.task,
                                                                       self.freeze.current_user.task_count + 1)
                    self.freeze.current_user.remove_task_by_id(scheduled_data.task_id)
                    self.freeze.current_user.add_task(copy_task)
            elif datetime.datetime.now() > delta_time and scheduled_data.repeat_count > 0:
                scheduled_data.date_now = datetime.datetime.now()
                copy_task = copy.deepcopy(scheduled_data.task)
                copy_task.scheduled_data = scheduler.ScheduledData(scheduled_data.repeat_time,
                                                                   scheduled_data.repeat_count,
                                                                   delta_time,
                                                                   scheduled_data.task,
                                                                   self.freeze.current_user.task_count + 1)
                self.freeze.current_user.remove_task_by_id(scheduled_data.task_id)
                self.freeze.current_user.add_task(copy_task)

        remove_list = list()
        past_deadline_str = 'Past deadline: '
        notify_str = 'Notify: '

        # Generate strings for notify command.

        for task in self.task_list:
            if task.is_temporary:
                if datetime.datetime.now() > task.date_end:
                    if task.priority_level == PriorityLevel.LOW and not task.is_notified:
                        past_deadline_str += task.name + f' |{PriorityLevel.LOW}| '
                        task.is_notified = True
                        remove_list.append(task)
                    elif task.priority_level == PriorityLevel.MEDIUM and not task.is_notified:
                        past_deadline_str += task.name + f' |{PriorityLevel.MEDIUM}| '
                        task.is_notified = True
                        remove_list.append(task)
                    elif task.priority_level == PriorityLevel.HIGH:
                        past_deadline_str += task.name + f' |{PriorityLevel.HIGH}| '
                        task.is_notified = True
                else:
                    if len(task.notify_list) > 0:
                        remove_notify_list = list()
                        for notify in task.notify_list:
                            if datetime.datetime.now() > notify:
                                notify_str += task.name + ' || '
                                remove_notify_list.append(notify)
                        for notify in remove_notify_list:
                            task.notify_list.remove(notify)

        for task in remove_list:
            self.task_list.remove(task)

        if not past_deadline_str == 'Past deadline: ':
            subprocess.Popen(['notify-send', 'Freeze Notification', past_deadline_str])

        time.sleep(5)

        if not notify_str == 'Notify: ':
            subprocess.Popen(['notify-send', 'Freeze Notification', notify_str])

        self.freeze.save_user_list(users_path=self.file_path)

        os.rename(self.file_path, self.freeze.config.users_path)
   #     subprocess.Popen(['mv', self.file_path, self.freeze.config.users_path])