from setuptools import setup, find_packages
from os.path import join, dirname
import main_freeze

setup(
    name="Freeze",
    version=main_freeze.__version__,
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    entry_points={
        'console_scripts':
        ['freeze = main_freeze.freeze:main']
    }, install_requires=['colorama']
)