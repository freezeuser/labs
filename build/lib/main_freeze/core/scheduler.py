class Scheduler(object):

    def __init__(self):
        self.task_count = 1
        self.schedule_data = dict()

    def add_schedule_task(self, task_obj, repeat_time, repeat_count):
        """
        Add schedule data tuple to the class dict.

        :param task_obj: Task obj that we need to schedule.
        :type task_obj: Task
        :param repeat_time:
        :type repeat_time: int
        :param repeat_count:
        :type repeat_count: int
        """
        if repeat_count is None:
            self.schedule_data[self.task_count] = {'task': task_obj, 'repeat_time': repeat_time, 'repeat_count': None}
        else:
            self.schedule_data[self.task_count] = {'task': task_obj, 'repeat_time': repeat_time,
                                                   'repeat_count': repeat_count}

        self.task_count += 1

    def get_schedule_task(self, task_id):
        schedule_task = self.schedule_data.get(task_id)

        if schedule_task is None:
            raise SystemExit('No such a schedule task')
        else:
            return schedule_task
