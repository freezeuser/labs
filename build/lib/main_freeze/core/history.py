import logging

logger = logging.getLogger(__name__)


class History(object):
    """
    Class for history storage
    """

    def __init__(self):
        super().__init__()
        logger.info('Creating History class')
        self.task_history = list()

    def add_task(self, task_tuple):
        """
        Method for adding a task into the task_history list
        :param task_tuple: [0] -> parent id, [1] -> task id, [2] -> task obj
        :type task_tuple: tuple
        :return:
        """
        logger.info('Appending task to the user history')
        self.task_history.append(task_tuple)

    def get_task(self, task_id):
        for task in self.task_history:
            if task[1] == task_id:
                return task
        return None

    def get_all_tasks(self):
        return self.task_history

    def remove_task(self, task_id):

        remove_task = self.get_task(task_id)
        try:
            self.task_history.remove(remove_task)
        except ValueError:
            raise SystemExit('No task with this id in history.')

    def clear_history(self):
        self.task_history.clear()