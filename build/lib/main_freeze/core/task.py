import datetime
import colorama
import copy
import logging
import string

logger = logging.getLogger(__name__)
date_format = '%Y-%m-%d %H:%M:%S'


class Task(object):
    """
    Class for representation tasks in program.
    """
    def __init__(self, name='default', date_start=None, date_end=None):
        """
        :param name: name/description of task
        :type name: str
        :param date_start: string representation of start datetime according to specified date_format
        :type date_start: str
        :param date_end: string representation of end datetime according to specified date_format
        :type date_end: str
        """
        self.task_dict = dict()

        self.name = name
        self.priority_level = PriorityLevel.LOW

        self.date_create = datetime.datetime.now()
        self.date_edit = None

        self.is_notified = False
        self.is_awarded = False
        self.is_scheduled = False

        self.notify_list = list()
        self.linked_tasks = list()
        self.tag_list = list()
        self.comments_list = list()
        self.permissions = list()

        self.scheduled_data = None

        if date_start is not None and date_end is not None:
            self.date_start = datetime.datetime.strptime(date_start, date_format)
            self.date_end = datetime.datetime.strptime(date_end, date_format)
            self.is_temporary = True
        else:
            self.date_start = None
            self.date_end = None
            self.is_temporary = False

    def add_task_by_id(self, search_id, task_obj, task_id, args):
        """
        Recursive addition task on specified search_id

        :param search_id: id to which task we need to add task_obj
        :type search_id: int
        :param task_obj: Task object that we will add on specified id
        :type task_obj: Task
        :param task_id: Id that task object will have after adding to task_dict
        :type task_id: int
        :param args:
        :return: Returns True if success, else False
        :rtype: bool
        """
        logger.info('Appending task by id')

        if self.task_dict:
            if not (self.task_dict.get(search_id) is None):
                self.task_dict[search_id].task_dict[task_id] = task_obj
                return True
            else:
                for key, task_item in self.task_dict.items():
                    if task_item.add_task_by_id(search_id, task_obj, task_id, args):
                        return True
        else:
            return False

    def remove_task_by_id(self, search_id, parent_id=None, force=False):
        """
        Recursive deletion on specified search_id

        :param search_id: Task id that method will remove
        :type search_id: int
        :param parent_id: id of the parent task
        :type parent_id: int
        :param force:
        :type force: bool
        :return: tuple with parent id, deleted object id and deleted object
        :rtype: tuple
        """
        logger.info('Removing task by id')

        if self.task_dict:
            if not (self.task_dict.get(search_id) is None):
                for link in self.task_dict[search_id].linked_tasks:
                    if not (self.find_task_by_id(task_id=link) is None):
                        raise SystemExit('Cannot delete this task, linked tasks: {0}'
                                         .format(self.task_dict[search_id].linked_tasks))
                deleted_obj = copy.deepcopy(self.task_dict[search_id])
                del self.task_dict[search_id]
                return parent_id, search_id, deleted_obj
            else:
                for key, task_item in self.task_dict.items():
                    deleted_obj = task_item.remove_task_by_id(search_id, key)
                    return deleted_obj
        else:
            return parent_id, search_id, None

    def append_tags_by_id(self, task_id, tags, remove=False):
        """
        Method for appending or removing (depends on remove flag) tags from task on the task_id

        :param task_id: Task id to which we adding tags
        :type task_id: int
        :param tags: list of tags we need to append/remove
        :type tags: list
        :param remove: Flag to check, that we need to append or remove tags
        :type remove: bool
        :raises ValueError: if removing tag that is not exist in tag list
        :return: True if success, else False
        :rtype: bool
        """
        logger.info('Appending tags by id')

        if self.task_dict:
            if not (self.task_dict.get(task_id) is None):
                if remove:
                    try:
                        for tag in tags:
                            self.task_dict[task_id].tag_list.remove(tag)
                    except ValueError:
                        raise SystemExit('There is no such a tag {0}'.format(tag))
                else:
                    self.task_dict[task_id].tag_list.extend(tags)
                return True
            else:
                for key, task_item in self.task_dict.items():
                    if task_item.append_tags_by_id(task_id, tags, remove):
                        return True
        else:
            return False

    def find_task_by_id(self, task_id):
        """
        Method for finding task by id.

        :param task_id:
        :type task_id: int
        :return: task with task_id or None
        :rtype: Task
        """
        logger.info('Find task by id')

        if not (self.task_dict.get(task_id) is None):
            return self.task_dict[task_id]
        else:
            for key, task_item in self.task_dict.items():
                search_task = task_item.find_task_by_id(task_id)
                if not (search_task is None):
                    return search_task
        return None

    def get_temporary_task_list(self):
        """
        :return: list of temporary tasks of the current task.
        """
        task_list = list()
        for key, value in self.task_dict.items():
            if value.is_temporary or len(value.notify_list) > 0:
                task_list.append(value)
            child_list = value.get_temporary_task_list()
            task_list.extend(child_list)
        return task_list

    def get_scheduled_task_list(self):
        """
        :return: list of scheduled task list of the current task.
        """
        task_list = list()
        for key, value in self.task_dict.items():
            if not (value.scheduled_data is None):
                task_list.append(value)
            child_list = value.get_scheduled_task_list()
            task_list.extend(child_list)
        return task_list

    def add_link(self, task_id):
        """
        Adding link to the task_id

        :param task_id:
        :type task_id: int
        """
        if not (task_id in self.linked_tasks):
            self.linked_tasks.append(task_id)
        else:
            raise SystemExit('Link already exists')

    def add_notify(self, notify_data):
        """
        Adding notify to the task with notify_data

        :param notify_data:
        :type notify_data: tuple
        :return:
        """
        if isinstance(notify_data, tuple):
            notify_data = self.date_end - datetime.timedelta(days=notify_data[0],
                                                             hours=notify_data[1], minutes=notify_data[2])
        if isinstance(notify_data, datetime.datetime) and not (notify_data in self.notify_list):
            if self.is_temporary and self.date_end < notify_data:
                    raise SystemExit('Wrong data')
            else:
                self.notify_list.append(notify_data)


class PriorityLevel(object):
    LOW = 1
    MEDIUM = 2
    HIGH = 3
